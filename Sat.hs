-- Advanced Programming, HW 3
-- by Achal Shah, achals
-- and Rushikumar Vyas, rvyas

{-# OPTIONS -Wall -Werror -fwarn-tabs -fno-warn-type-defaults #-} 
{-# LANGUAGE FlexibleInstances #-} 

module Sat where

import Data.Map (Map)  -- use finite map data structure from standard library
import qualified Data.Map as Map
import Data.List (find)


import Test.QuickCheck
import Control.Monad( liftM2)

-- | An expression in CNF, the conjunction of clauses
newtype CNF = CNF [ Clause ] deriving (Eq, Ord, Show)
unCNF :: CNF -> [ Clause ]
unCNF (CNF cs) = cs

-- | A clause -- the disjunction of a number of literals
type Clause = [ Lit ]

-- | A literal, either a positive or negative variable
data Lit = Lit Bool Var deriving (Eq, Ord, Show)

-- | A variable 
data Var = A | B | C | D | E | F | G | H | I | J 
  deriving (Read, Eq, Ord, Show, Enum, Bounded)

-- | invert the polarity of a literal
invert :: Lit -> Lit
invert (Lit b x) = Lit (not b) x



exampleFormula :: CNF
--exampleFormula = CNF [[Lit True A],[Lit True B,Lit False A]]
--exampleFormula = CNF [[Lit False A],[Lit True B,Lit False A],[Lit True B,Lit False A]]
exampleFormula = CNF [[Lit False B],[Lit False A,Lit True B,Lit True D],[Lit False E,Lit False J,Lit False G],[Lit False G,Lit True G,Lit True C],[Lit True B,Lit False I,Lit False A]]

exampleAssignment :: Map Var Bool
--exampleAssignment = Map.fromList [(C, False), (D, True)]
exampleAssignment = Map.fromList []

exampleSolver :: Solver
exampleSolver = Solver (unCNF exampleFormula) exampleAssignment

interpLit :: Map Var Bool -> Lit -> Bool
interpLit m (Lit b k) = maybe True (if b then id else not)  (Map.lookup k m)

interp :: Map Var Bool -> CNF -> Bool
interp m (CNF formula) = all (any (interpLit m)) formula where

prop_interpExample :: Bool
prop_interpExample = interp exampleAssignment exampleFormula

dpll :: CNF -> Maybe (Map Var Bool)
dpll (CNF cnf) = loop initial where
  initial :: Solver
  initial = Solver cnf Map.empty
  loop :: Solver -> Maybe (Map Var Bool)
  loop s  = solverAux s

data Solver = Solver { clauses :: [Clause] , assignment :: (Map Var Bool) }
  deriving (Eq, Ord, Show)

-- | If a clause is a unit clause, i.e. it contains only a single
-- unassigned literal, this clause can only be satisfied by assigning
-- the necessary value to make this literal true. Unit propagation updates
-- the map with the assignment and simplifies the clauses to reflect this 
-- reasoning.

solverAux :: Solver -> Maybe (Map Var Bool)
solverAux s 
          | clauses newSolver==[] = Just (assignment newSolver)
          | find (\c -> c==[] )(clauses newSolver) /= Nothing  = Nothing
          | otherwise = recurseDpll newSolver
          where 
          newSolver = (pureLitAssign (unitPropagate s))
          recurseDpll :: Solver -> Maybe (Map Var Bool)
          recurseDpll solverInst
                      | chooseLit solverInst == Nothing = Just (assignment solverInst)
                      | solverAux (trueAssign (chooseLit solverInst) solverInst) /= Nothing  = solverAux (trueAssign (chooseLit solverInst) solverInst)
                      | solverAux (falseAssign (chooseLit solverInst) solverInst) /= Nothing =  (solverAux (falseAssign (chooseLit solverInst) solverInst))
                      | otherwise = Nothing
          chooseLit :: Solver -> Maybe Lit
          chooseLit (Solver ( [(Lit b v)]: _ ) _) = Just (Lit b v)
          chooseLit _ = Nothing

 
          trueAssign :: Maybe Lit -> Solver -> Solver
          trueAssign (Just (Lit _ v)) solverInstance
                      = (Solver (updateClauses (clauses solverInstance) (Lit True v)) 
                                (Map.insert v True (assignment solverInstance)))
          trueAssign _ _ = Solver [[]] Map.empty

          falseAssign :: Maybe Lit -> Solver -> Solver
          falseAssign (Just (Lit _ v))  solverInstance
                      = (Solver (updateClauses (clauses solverInstance) (Lit False v)) 
                                (Map.insert v False (assignment solverInstance)))
          falseAssign _ _ = Solver [[]] Map.empty
                        
--solverAux  (Solver clauseList assignmentList) = where solverInstance =
          

unitPropagate :: Solver -> Solver 
unitPropagate (Solver [] currMap) = (Solver [] currMap)
unitPropagate solverInstance 
              | has_unit solverInstance == False = solverInstance
              | otherwise = unitPropagate (unitPropAux solverInstance [])

unitPropAux :: Solver -> [Clause] -> Solver
unitPropAux (Solver [] currMap) finalClauses = (Solver finalClauses currMap)
unitPropAux (Solver (x:xs) currMap) finalClauses
            | length x == 1 = unitPropAux (Solver (updateClauses xs (head x)) (updateMap currMap x)) (updateClauses finalClauses (head x))
            | otherwise     = unitPropAux (Solver (xs) currMap) (x:finalClauses)

updateMap :: Map Var Bool -> Clause -> Map Var Bool
updateMap currMap [(Lit boolVal varName)] = Map.insert varName boolVal currMap
updateMap currMap _ = currMap

updateClauses :: [Clause] -> Lit -> [Clause]
updateClauses [] _ = []
updateClauses xs lit 
              = map (updateClause lit) (filter (not . checkClause lit) xs)
              
checkClause :: Lit-> Clause -> Bool
checkClause _ [] = False
checkClause lit xs = any (\litTemp->lit==litTemp) xs

updateClause :: Lit -> Clause ->  Clause
updateClause _ [] = []
updateClause (Lit boolVal varName) xs
             = filter (\litT -> not (litT ==(Lit (not boolVal) varName))) xs

-- | After unit propagation, solver state should contain no more unit 
-- clauses.

has_unit :: Solver -> Bool
has_unit s = 
  let cs = (clauses s) in
    any (\c -> length c == 1) cs

prop_unitPropagate :: Solver -> Bool
prop_unitPropagate s = 
  let cs = clauses (unitPropagate s) in
    all (\c -> length c /= 1) cs

ifPureLiteral :: [Lit] -> (Map Var Bool) -> (Map Var Bool) -> ((Map Var Bool),(Map Var Bool))
ifPureLiteral [] mapPureLiteral mapNotPureLiteral = (mapPureLiteral,mapNotPureLiteral)
ifPureLiteral ((Lit b v):xs) mapPureLiteral mapNotPureLiteral = if not (Map.member v mapNotPureLiteral) then
                                                                   if (tValue == Nothing) then ifPureLiteral xs (Map.insert v b mapPureLiteral) mapNotPureLiteral 
                                                                   else
                                                                     if tValue /= (Just b) then ifPureLiteral xs (Map.delete v mapPureLiteral) (Map.insert v b mapNotPureLiteral)
                                                                     else ifPureLiteral xs mapPureLiteral mapNotPureLiteral
                                                                else ifPureLiteral xs mapPureLiteral mapNotPureLiteral
                                                                where tValue = ( Map.lookup v mapPureLiteral)

find_pureLiteral :: [Clause] -> (Map Var Bool) -> (Map Var Bool) -> (Map Var Bool)
find_pureLiteral [] mapPureLiteral _     = mapPureLiteral
find_pureLiteral (x:xs) mapPureLiteral mapNotPureLiteral = find_pureLiteral xs (fst(literals)) (snd(literals))
                                                           where literals = (ifPureLiteral x mapPureLiteral mapNotPureLiteral)

-- | If a propositional variable occurs with only one polarity in the
-- formula, it is called pure. Pure literals can always be assigned in
-- a way that makes all clauses containing them true. Thus, these
-- clauses do not constrain the search anymore and can be deleted. 
checkPureLiteral :: Clause -> (Map Var Bool) -> Bool
checkPureLiteral [] _ = False
checkPureLiteral ((Lit _ varName):xs) mapPureLiteral = if (Map.member varName mapPureLiteral) then True else checkPureLiteral xs mapPureLiteral


updatePureLiteralClauses :: [Clause] -> (Map Var Bool) -> [Clause]
updatePureLiteralClauses [] _ = []
updatePureLiteralClauses (x:xs) mapPureLiteral = if (checkPureLiteral x mapPureLiteral) then updatePureLiteralClauses xs mapPureLiteral
                                                 else x:(updatePureLiteralClauses xs mapPureLiteral)

--recurseLitAssign :: Solver -> Solver -> Solver
--recurseLitAssign (Solver clauseList assignmentList) (Solver oriClauseList oriAssignmentList) = (Solver (updatePureLiteralClauses clauseList pureLiteral) (Map.union pureLiteral assignmentList))
                                                                                                --where pureLiteral = find_pureLiteral clauseList (Map.fromList []) (Map.fromList [])

pureLitAssign :: Solver -> Solver
pureLitAssign (Solver clauseList assignmentList ) 
                    | isPureLitAssign (Solver clauseList assignmentList ) == True = (Solver clauseList assignmentList )
                    | otherwise = pureLitAssign (Solver (updatePureLiteralClauses clauseList pureLiteral) (Map.union pureLiteral assignmentList))
                    where pureLiteral = find_pureLiteral clauseList (Map.fromList []) (Map.fromList [])

isPureLitAssign :: Solver -> Bool
isPureLitAssign s =
   let cs = clauses (s) in 
   all (all (\l -> invert l `elem` concat cs)) cs
-- | After pure literal assignment, the solver state should not contain 
-- any pure literals
prop_pureLitAssign :: Solver -> Bool
prop_pureLitAssign s = 
   let cs = clauses (pureLitAssign s) in 
   all (all (\l -> invert l `elem` concat cs)) cs



prop_dpll :: CNF -> Property
prop_dpll c = 
  case dpll c of 
    Just m  ->  property (interp m c)
    Nothing ->  property True  
  
instance Arbitrary Var where
   arbitrary = elements [A, B, C, D, E, F, G, H, I, J]

instance Arbitrary Lit where
   arbitrary = do
             x <- oneof [arbitrary :: Gen Bool]
             y <- arbitrary:: Gen Var
             return (Lit x y)

genList ::  (Arbitrary a) => Gen [a]
genList = frequency [ (1, return []), 
                       (5, liftM2 (:) arbitrary genList)]
             

instance Arbitrary CNF where
     arbitrary = do
               x <- oneof [genList::Gen [Lit]]
               return (CNF [x])


instance Arbitrary Solver where
   arbitrary = do
             x <- genList :: Gen [Clause]
             return (Solver x Map.empty)



main :: IO ()
main = do 
  quickCheck prop_interpExample
  quickCheck prop_unitPropagate
  quickCheck prop_pureLitAssign
  quickCheck prop_dpll

